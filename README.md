# Relative Document:

**MultiViews Document**(MultiWindows4UE Plugin has the same MultiViews Settings)**: DOC for MultiViews Setting**

[https://gitlab.com/UE4MarketplaceSubmission_Public/MultiViews4UE4_Public](https://redirect.epicgames.com/?redirectTo=https://gitlab.com/UE4MarketplaceSubmission_Public/MultiViews4UE4_Public)

# User Guide of "MultiWindows4UE4" Plugin 

**Demo Video**: [https://youtu.be/SfOjjzGyvCY](https://youtu.be/SfOjjzGyvCY)

![00_VideoDemo_Image_02](./README.res/01_Images/00_VideoDemo_Image_02.PNG)


## 1  Quick use guide
---
### 1.1  Download

#### 1.1.1 Download Plugin

Download the "MultiWindows4UE4" Plugin from UE4 Marketplace: https://www.unrealengine.com/marketplace/zh-CN/product/8cff7837a1214799ba4c628949c51dcb

Such as download the UE4.24 version "MultiWindows4UE4" Plugin.

#### 1.1.2 Prepare your UE4 project

You can use your own UE4 project or download the UE4 project demo from:
https://gitlab.com/UE4MarketplaceSubmission_Public/multiwindows4ue4_public

---
### 1.2 Plugin and UE4 Project

#### 1.2.1 Copy Plugin to your UE4 Project

+ Find the downloaded "MultiWindows4UE4" Plugin from your UE4 Engine folder(The same version to the Plugin, For example, the Plugin version we downloaded earlier is UE4.24): "`\UE_4.24\Engine\Plugins\Marketplace`". 

+ Copy the "MultiWindows4UE4" plugin folder to the "Plugins" folder of your UE4 project, for example:
"`\MultiWindows4UE4_Public\01_Codes\MultiWindows4UE4Demo\Plugins`"

#### 1.2.2 Switch the Project Demo version

Switch the project version to the same version as the plugin.

![SwitchProjectDemoVersion](./README.res/01_Images/01_SwitchProjectDemoVersion_01.png)


---

### 1.3 Config "MultiWindows4UE4" Plugin 

Configure the "MultiWindows4UE4" plugin in the "`DefaultEngine.ini`" config file of your UE4 Project.

```ini
[/Script/Engine.Engine]
GameEngine=/Script/MultiWindows4UE4.MultiWindowsGameEngine
UnrealEdEngine=/Script/MultiWindows4UE4Editor.MultiWindowsUnrealEdEngine
GameViewportClientClassName=/Script/MultiWindows4UE4.MultiWindowsGameViewportClient
LocalPlayerClassName=/Script/MultiWindows4UE4.MultiWindowsLocalPlayer
```

![ConfigPluginInConfigFile](./README.res/01_Images/02_ConfigPluginInConfigFile_01.png)

---

### 1.4 Enable the "MultiWindows4UE4" Plugin in the "**Plugins Browser Tab**" of UE4 Project Editor

Open the "**Plugins Browser Tab**": 
    
![04_EnablePlugin_01](./README.res/01_Images/04_EnablePlugin_01.png)


Make sure the "MultiWindows4UE4" Plugin is "Enabled": 
    
![04_EnablePlugin_02](./README.res/01_Images/04_EnablePlugin_02.png)

### 1.5 Create New ViewportWindow


You can view the blueprint code of the image below in "[BlueprintUE.com](https://blueprintue.com/blueprint/cr1qv3ch/)": https://blueprintue.com/blueprint/cr1qv3ch/

![05_CreateNewViewportWindow_01](./README.res/01_Images/05_CreateNewViewportWindow_01.png)


### 1.6 Set the Position of One Window and Resize one Window

#### 1.6.1 A example to set the Size and Position of a Window

After you create new Window. You can set the new Postion of the Window and resize the window.

Such as you want to set the size and position of the new created Window (Its Index is 0) to 500x500 and (0, 0), set it like this.

![08_ResizeWindow_01](./README.res/01_Images/08_ResizeWindow_01.png)

Blueprint Code is like this.

![08_ResizeWindow_02](./README.res/01_Images/08_ResizeWindow_02.png)

#### 1.6.2 Multi-Monitor: Auto Create new fullscreen Window for Other Monitor. Test this use "Standalone Game", not PIE.

See the Blueprint example:

Image On "imgbb.com":
https://ibb.co/HXPbQWk

Or

![09_FullscreenToOtherMonitor_00](./README.res/01_Images/09_FullscreenToOtherMonitor_00.png)

This image is a screenshot by "Blueprint Graph Screenshot" plugin:"https://www.unrealengine.com/marketplace/en-US/product/blueprint-graph-screenshot"

The same Blueprint Code is not showed correctly on BlueprintUE.com:
https://blueprintue.com/blueprint/9wlohz8h/



### 1.7 How to fix the issues of Multi Views Splicing

#### 1. Set the Screen Resolution  of Windows System to 100%, not 150% or others

#### 2. Close some Post-Process effects

- Close the Vignette(Set the value of Vignette to 0 in the PostProcessVolume or Camera PostProcess)
- Close Screen-Space AO
- Close Screen-Space Reflection
- Close Motion Blur: In the ProjectSettings->Rendering
- Close **Auto Exposure**: In the ProjectSettings->Rendering
- Close Bloom: In the ProjectSettings->Rendering

#### 3. Use absolute position for setting the location of Views，to avoid pixel lost after "float" converted to "int".

### 1.8 How to Fix the "Cylindrical surface distortion" after Multi Views Splicing

#### Method01: Use the "Panini Projection" .

See "Panini Projection" 

https://docs.unrealengine.com/en-US/RenderingAndGraphics/PostProcessEffects/PaniniProjection/index.html

#### Method02: Use Custom PostProcess Material



### 1.9 How to Close Window(Supported by UE4.26.1 Version Plugin)

NOTE: "CloseWindow" is Supported by UE4.26.1 Version Plugin.

This is a example in the Demo Project:
![10_CloseWindow_01](./README.res/01_Images/10_CloseWindow_01.png)

![10_CloseWindow_02](./README.res/01_Images/10_CloseWindow_02.png)



### 1.20 How to use "Bind to ViewTarget" ViewpointType in the "ViewManager"?

#### Example 01: 

##### Issue Description:

![image-20231031221556350](README/00_Res/01_Images/image-20231031221556350.png)

##### Code Example:

###### Winodw Manager Actor: "BPA_MultiWindows"

![image-20231031222938480](README/00_Res/01_Images/image-20231031222938480.png)

###### ViewTargetActor is a "BPA_Camera" Actor which is attached to the Character's Camera Component:

![image-20231031223017540](README/00_Res/01_Images/image-20231031223017540.png)

![image-20231031223234662](README/00_Res/01_Images/image-20231031223234662.png)

###### Configure the "ViewTarget" of "ViewManaer" for the "BPA_MultiWindows" Actor in Scene "Outliner":

![image-20231031223424505](README/00_Res/01_Images/image-20231031223424505.png)

###### Result: Left Window is First-Person Perspective; Right Window is Third-Person Perspective.

![image-20231031223634005](README/00_Res/01_Images/image-20231031223634005.png)



# Issues And Solutions

## 01 Opening multiple windows at once, some of them displayed a brief white screen. 

### Description: Creating multiple windows immediately upon launching the game might lead to the creation of additional windows before the original main window is established. These additional windows may display a brief white screen. 

![image-20240303222419579](README/00_Res/01_Images/image-20240303222419579.png)

![image-20240303222056325](README/00_Res/01_Images/image-20240303222056325.png)

### Solution: The solution is to first create the original main window, hence delaying the creation of other windows for a short period. Consider not creating multiple windows immediately upon starting the game. Instead, delay their creation by a bit of time, such as using the Delay blueprint function to wait for 1 second before creating additional windows.

![image-20240303222444003](README/00_Res/01_Images/image-20240303222444003.png)

![image-20240303222532940](README/00_Res/01_Images/image-20240303222532940.png)

![image-20240303222557913](README/00_Res/01_Images/image-20240303222557913.png)
